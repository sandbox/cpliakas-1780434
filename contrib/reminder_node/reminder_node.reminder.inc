<?php

/**
 * @file
 * Reminder hook implementations.
 */

/**
 * Implements hook_reminder_ENTITY_TYPE_date_fields() for node entities.
 */
function reminder_node_reminder_node_date_fields($bundle_name) {
  $fields = array();

  $fields['created'] = array(
    'label' => t('Published date'),
    'value callback' => 'reminder_get_entity_property_value',
    'settings callback' => FALSE,
    'group' => t('Node property'),
  );

  $fields['changed'] = array(
    'label' => t('Updated date'),
    'value callback' => 'reminder_get_entity_property_value',
    'settings callback' => FALSE,
    'group' => t('Node property'),
  );

  $fields += reminder_get_module_fields('node', $bundle_name, 'date');

  return $fields;
}

/**
 * Implements hook_reminder_ENTITY_TYPE_recipient_fields() for node entities.
 */
function reminder_node_reminder_node_recipient_fields($bundle_name) {
  $fields = array();

  $fields['mail'] = array(
    'label' => t('Email of node author'),
    'value callback' => 'reminder_get_node_author_property_value',
    'settings callback' => FALSE,
    'group' => t('User property'),
  );

  $fields += reminder_get_module_fields('node', $bundle_name, 'email');

  return $fields;
}

/**
 * A "value callback" that extracts a property from the node's author.
 */
function reminder_get_node_author_property_value($node, $field_name) {
  if ($account = user_load($node->uid)) {
    return isset($account->$field_name) ? $account->$field_name : FALSE;
  }
  return FALSE;
}

/**
 * Implements hook_reminder_recipient_values().
 */
function reminder_node_reminder_recipient_values($node) {
  $recipient_field = variable_get('reminder_recipient_field_' . $node->type, 'mail');
  return reminder_get_recipient_values($node, 'node', $node->type, $recipient_field, 'recipient');
}

/**
 * Implements hook_reminder_message_token_replace().
 */
function reminder_node_reminder_message_token_replace($message, $recipient, $reminder, $entity) {
  $data = array(
    'node' => $entity,
  );
  return token_replace($message, $data);
}

/**
 * Implements hook_reminder_type_delete().
 *
 * Removes the bundle from the reminder_types_BUNDLE_NAME variable. The variable
 * is set in the reminder_form_node_type_form_alter() function.
 *
 * @see reminder_form_node_type_form_alter()
 */
function reminder_node_reminder_type_delete($type) {
  foreach (node_type_get_types() as $bundle_name => $bundle_label) {
    $variable_name = 'reminder_types_' . $bundle_name;
    $reminder_types = variable_get($variable_name, array());
    if (FALSE !== ($key = array_search($type, $reminder_types))) {
      unset($reminder_types[$key]);
      if ($reminder_types) {
        variable_set($variable_name, $reminder_types);
      }
      else {
        variable_del($variable_name);
        variable_del('reminder_enabled_' . $bundle_name);;
      }
    }
  }
}
