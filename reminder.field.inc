<?php

/**
 * @file
 * CRUD functions for the Reminder module.
 */

/**
 * Invokes a reminder hook to get fields.
 *
 * @param string $entity_type
 *   The machine name of the entity, i.e. "node", "user".
 * @param string $bundle_name
 *   The machine name of the bundle.
 * @param string $field_type
 *   The type of field being retrieved, i.e. "date", "recipient".
 *
 * @return array
 *   An array of recipient fields. Each date field is an array keyed by the
 *   machine readable name of the field to an associative array containing:
 *   - label:
 *   - value callback:
 *   - settings callback:
 *   - group:
 */
function reminder_get_fields($entity_type, $bundle_name, $field_type) {
  $return = array();
  $hook = 'reminder_' . $entity_type . '_' . $field_type . '_fields';
  foreach (module_implements($hook) as $module) {
    $function = $module . '_' . $hook;
    if ($fields = $function($bundle_name)) {
      foreach ($fields as $field_name => $field) {
        $return[$field_name] = $field + array(
          'label' => $field_name,
          'value callback' => FALSE,
          'settings callback' => FALSE,
          'group' => t('Other'),
        );
      }
    }
  }
  return $return;
}

/**
 * Builds an array of field options that can be used in FAPI arrays.
 *
 * @param string $entity_type
 *   The machine name of the entity, i.e. "node", "user".
 * @param string $bundle_name
 *   The machine name of the bundle.
 * @param string $field_type
 *   The type of field that options are being retrieved for, i.e. "date",
 *   "recipient".
 *
 * @return array
 *   An array of fields keyed by group. Each field is an associative array keyed
 *   by machine name of the field to the sanitized label.
 *
 * @see reminder_get_fields()
 */
function reminder_get_field_options($entity_type, $bundle_name, $field_type) {
  $options = array();
  $fields = reminder_get_fields($entity_type, $bundle_name, $field_type);
  foreach ($fields as $field_name => $field) {
    $options[$field['group']][$field_name] = check_plain($field['label']);
  }
  return $options;
}

/**
 * Extracts a value from an entity via the field's "value callback".
 *
 * @param stdClass $entity
 *   The entity that the date value is being extracted from.
 * @param string $entity_type
 *   The machine name of the entity, i.e. "node", "user".
 * @param string $bundle_name
 *   The machine name of the bundle.
 * @param string $field_name
 *   The machine name of the field.
 * @param string $field_type
 *   The type of field that the value is being retrieved from, i.e. "date",
 *   "recipient".
 *
 * return mixed|FALSE
 *   The field value, FALSE if the date couldn't be extracted.
 */
function reminder_get_field_value($entity, $entity_type, $bundle_name, $field_name, $field_type) {
  $fields = reminder_get_fields($entity_type, $bundle_name, $field_type);
  if (isset($fields[$field_name])) {
    return call_user_func($fields[$field_name]['value callback'], $entity, $field_name);
  }
  return FALSE;
}

/**
 * Helper function that gets field instances associated with a module that are
 * attached to the passed entity's bundle.
 *
 * @param string $entity_type
 *   The machine name of the entity, i.e. "node", "user".
 * @param string $bundle_name
 *   The machine name of the bundle.
 * @param string $module
 *   The module fields are being returned for.
 *
 * @return array
 *   An array of field info. Each field is an array keyed by the machine
 *   readable name of the field to an associative array containing:
 *   - label:
 *   - value callback:
 *   - settings callback:
 *   - group:
 */
function reminder_get_module_fields($entity_type, $bundle_name, $module) {
  $fields = array();
  if (function_exists('field_info_fields')) {
    $field_info = field_info_fields();
    $instances = field_info_instances($entity_type, $bundle_name);
    foreach ($instances as $field_name => $instance) {
      if ($field_info[$field_name]['module'] == $module) {
        $fields[$field_name] = array(
          'label' => check_plain($instance['label']) . ' (' . check_plain($field_name) . ')',
          'value callback' => 'reminder_get_' . $module . '_field_value',
          'settings callback' => FALSE,
          'group' => t('Field'),
        );
      }
    }
  }
  return $fields;
}

/**
 * A "value callback" that extracts a date value from a date field.
 *
 * @todo handle end date and multiple values.
 */
function reminder_get_date_field_value($entity, $field_name) {
  if (!empty($entity->$field_name)) {
    $field = $entity->$field_name;
    list($lang, $values) = each($field);

    $tz = new DateTimeZone(isset($field['timezone']) ? $field['timezone'] : 'UTC');
    foreach ($values as $value) {
      if ($date = date_create($value['value'], $tz)) {
        return $date->format('U');
      }
      else {
        return FALSE;
      }
    }
  }

  return FALSE;
}

/**
 * A "value callback" that extracts a date value from a date field.
 *
 * @todo handle end date and multiple values.
 */
function reminder_get_email_field_value($entity, $field_name) {
  $emails = array();
  if (!empty($entity->$field_name)) {
    $field = $entity->$field_name;
    list($lang, $values) = each($field);
    foreach ($values as $value) {
      $emails[] = $value['email'];
    }
  }
  return $emails;
}
