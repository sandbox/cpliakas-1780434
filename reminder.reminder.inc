<?php

/**
 * @file
 * Reminder hook implementations.
 */

/**
 * Implements hook_reminder_delivery_methods().
 */
function reminder_reminder_delivery_methods() {
  $delivery_methods = array();

  $delivery_methods['mail'] = array(
    'label' => t('Email'),
    'delivery callback' => 'reminder_send_mail',
  );

  $delivery_methods['watchdog'] = array(
    'label' => t('Watchdog'),
    'delivery callback' => 'reminder_send_watchdog',
  );

  return $delivery_methods;
}

/**
 * Delivery callback that sends messages to recipients via drupal_mail().
 *
 * @param string $message
 *   The message that is being sent to the recipients.
 * @param array $recipients
 *   An array of recipients messages are being sent to.
 * @param stdClass $reminder
 *   The reminder entity.
 * @param stdClass $entity
 *   The entity that the reminder references.
 */
function reminder_send_mail($message, array $recipients, $reminder, $entity) {
  global $language;
  foreach ($recipients as $recipient) {
    $params = array(
      'body' => $message,
      'recipient' => $recipient,
      'reminder' => $reminder,
      'entity' => $entity,
    );
    drupal_mail('reminder', 'reminder', $recipient, $language->language, $params);
  }
}

/**
 * Implements hook_mail().
 *
 * @todo Hook to format the mail.
 */
function reminder_mail($key, &$message, $params) {
  if ('reminder' == $key) {

    // @todo Make this configurable.
    $message['subject'] = t('Message from NDA Central');

    if (isset($params['body'])) {
      $body = reminder_message_token_replace($params['body'], $params['recipient'], $params['reminder'] ,$params['entity']);
      $message['body'][] = $body;
    }
  }
}

/**
 * Delivery callback that logs messages via watchdog for debugging.
 *
 * @param string $message
 *   The message that is being sent to the recipients.
 * @param array $recipients
 *   An array of recipients messages are being sent to.
 * @param stdClass $reminder
 *   The reminder entity.
 * @param stdClass $entity
 *   The entity that the reminder references.
 */
function reminder_send_watchdog($message, array $recipients, $reminder, $entity) {
  foreach ($recipients as $recipient) {
    $args = array(
      '@recipient' => $recipient,
      '!message' => reminder_message_token_replace($message, $recipients, $reminder, $entity),
    );
    watchdog('reminder', 'Reminder message sent to @recipient:<p>!message</p>', $args, WATCHDOG_DEBUG);
  }
}
