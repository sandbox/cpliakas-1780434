(function ($) {

Drupal.behaviors.reminderAdmin = {
  attach: function (context) {
    $('fieldset#edit-reminder', context).drupalSetSummary(function (context) {

      // Check whether reminders are enabled, set appropriate summary message.
      if ($('#edit-reminder-enabled').is(':checked')) {
        var vals = [];
        $("input[name^='reminder_types']:checked", context).parent().each(function() {
          vals.push(Drupal.checkPlain($(this).text()));
        });
        if (vals.length) {
          return vals.join(', ');
        }
        else {
          return Drupal.t('No reminders selected');
        }
      }
      else {
        return Drupal.t('Don\'t send reminders');
      }
    });
  }
};

})(jQuery);
