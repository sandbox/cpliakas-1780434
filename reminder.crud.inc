<?php

/**
 * @file
 * CRUD functions for the Reminder module.
 */

// http://commerceguys.com/blog/checking-out-entityfieldquery

/**
 * Encodes a machine name for use in a path.
 *
 * @param string $name
 *   The machine name being encoded.
 *
 * @return string
 *   The encoded machine name.
 */
function reminder_encode_name($name) {
  return str_replace('_', '-', $name);
}

/**
 * Decodes a machine name passed through the path.
 *
 * @param string $name
 *   The machine name passed through the path.
 *
 * @return string
 *   The decoded machine name.
 */
function reminder_decode_name($name) {
  return str_replace('-', '_', $name);
}

/**
 * Load a reminder object from the database.
 *
 * @param int $rid
 *   The unique identifier of the reminder.
 * @param int|NULL $sid
 *   The status ID, which is the entity's revision ID. Pass NULL to load the
 *   latest revision.
 * @param bool $reset
 *   Whether to reset the reminder_load_multiple cache, defaults to FALSE.
 *
 * @return stdClass|FALSE
 *   The reminder entity, FALSE of it could not be loaded.
 *
 * @see reminder_load_multiple()
 */
function reminder_load($rid = NULL, $sid = NULL, $reset = FALSE) {
  $rids = (isset($rid) ? array($rid) : array());
  $conditions = (isset($sid) ? array('sid' => $sid) : array());
  $reminder = reminder_load_multiple($rids, $conditions, $reset);
  return $reminder ? reset($reminder) : FALSE;
}

/**
 * Load reminder entities from the database.
 *
 * This function should be used whenever you need to load more than one reminder
 * from the database. Reminders are loaded into memory and will not require
 * database access if loaded again during the same page request.
 *
 * @see entity_load()
 * @see EntityFieldQuery
 *
 * @param array $rids
 *   An array of reminder IDs.
 * @param array $conditions
 *   (deprecated) An associative array of conditions on the {reminder} table,
 *   where the keys are the database fields and the values are the values those
 *   fields must have. Instead, it is preferable to use EntityFieldQuery to
 *   retrieve a list of entity IDs loadable by this function.
 * @param bool $reset
 *   Whether to reset the internal node_load cache.
 *
 * @return
 *   An array of reminder objects indexed by rid.
 *
 * @todo Remove $conditions in Drupal 8.
 */
function reminder_load_multiple(array $rids = array(), array $conditions = array(), $reset = FALSE) {
  return entity_load('reminder', $rids, $conditions, $reset);
}

/**
 * Loads reminder entities that are pending to be queued.
 *
 * @param int $limit
 *   The maximum number of reminders to return, defaults to 50.
 *
 * @return array
 *   An array of reminder entities keyed by rid.
 */
function reminder_load_pending($limit = 50, $request_time = REQUEST_TIME) {
  return entity_get_controller('reminder')->loadPending($limit, $request_time);
}

/**
 * Loads reminder entities based on the entities they reference.
 *
 * @param string $entity_type
 *   The machine name of the entity type.
 * @param int $entity_id
 *   The unique identifier of the entity.
 *
 * @return array
 *   An array of reminder entities keyed by rid.
 */
function reminder_load_by_entity($entity_type, $entity_id) {
  return entity_get_controller('reminder')->loadByEntity($entity_type, $entity_id);
}

/**
 * Creates or updates a reminder entity.
 *
 * @param stdClass|array $reminder
 *   The reminder entity being acted upon.
 *
 * @throws Exception
 */
function reminder_save($reminder) {
  entity_get_controller('reminder')->save($reminder);
}

/**
 * Deletes a reminder entity from the database.
 *
 * @param int $rid
 *   The unique identifier of the reminder.
 *
 * @throws Exception
 *
 * @see reminder_delete_multiple()
 */
function reminder_delete($rid) {
  reminder_delete_multiple(array($rid));
}

/**
 * Deletes multiple reminders from the database.
 *
 * @param array $rids
 *   An array of reminder IDs.
 *
 * @throws Exception
 */
function reminder_delete_multiple(array $rids) {
  entity_get_controller('reminder')->delete($rids);
}

/**
 * Helper function that sets a reminder status in a new revision.
 *
 * @param stdClass $reminder
 *   The reminder entity.
 * @param string $status
 *   The status, see the REMINDER_STATUS_* constants.
 *
 * @return bool
 *   The success of the operation.
 */
function reminder_save_status($reminder, $status) {
  // Don't do anything if the status is the same.
  if ($reminder->status == $status) {
    return TRUE;
  }

  try {
    $reminder->revision = TRUE;
    $reminder->status = $status;
    reminder_save($reminder);
    return TRUE;
  }
  catch (Exception $e) {
    if (user_access('administer reminders')) {
      $args = array(
        '%rid' =>  $reminder->rid,
        '%status' => $status,
      );
      $message = t('Error setting status of reminder %rid to %status.', $args);
      drupal_set_message($message, 'error');
    }
  }
  return FALSE;
}

/**
 * Returns an initialized reminder.
 *
 * @param bool $object
 *   Whether to return reminder as an object, defaults to FALSE which returns
 *   the reminder as an array.
 *
 * @return stdClass|array
 *   The reminder object or array.
 */
function reminder_type_new($object = FALSE) {
  $array = array(
    'type' => '',
    'label' => '',
    'has_description' => 0,
    'description' => '',
    'message' => '',
    'delivery_method' => 'mail',
    'intervals' => array(),
  );
  return (!$object) ? $array : (object) $array;
}

/**
 * Loads all reminder types from the database.
 *
 * This functionality cannot be put in the controller because we get infinite
 * loops on install. This is because we need to loop over the bundles in
 * reminder_entity_info().
 *
 * @return array
 *   An associative array keyed by machine name to reminder type.
 */
function reminder_type_load_all() {
  $reminder_types = &drupal_static(__FUNCTION__);
  if (NULL === $reminder_types) {
    try {
      $reminder_types = array();
      $result = db_query('SELECT * FROM {reminder_type} ORDER BY type');
      foreach ($result as $record) {
        $record->intervals = unserialize($record->intervals);
        $reminder_types[$record->type] = $record;
      }
    }
    catch (Exception $e) {
      watchdog_exception('reminder', $e);
    }
  }
  return $reminder_types;
}

/**
 * Loads a reminder type object.
 *
 * @param string $type
 *   The machine name of the reminder type.
 *
 * @return stdClass|FALSE
 *   The reminder type, FALSE if the passed type does not exist.
 */
function reminder_type_load($type) {
  $type = reminder_decode_name($type);
  return entity_get_controller('reminder')->loadBundle($type);
}

/**
 * Gets all reminder types keyed my machine name to label.
 *
 * The labels are passed through check_plain(), so they can be used directly in
 * FAPI arrays.
 *
 * @return array
 *   An associative array keyed by machine name to label.
 */
function reminder_type_get_options() {
  $options = array();
  foreach (reminder_type_load_all() as $type => $reminder_type) {
    $options[$type] = check_plain($reminder_type->label);
  }
  return $options;
}

/**
 * Saves a reminder type.
 *
 * @param stdClass|array $reminder
 *   The reminder type being saved.
 *
 * @return int
 *   The status flag indicating outcome of the operation. See the SAVED_*
 *   constants for possible values.
 *
 * @throws Exception
 */
function reminder_type_save($reminder_type) {
  $status = entity_get_controller('reminder')->saveBundle($reminder_type);
  drupal_static_reset('reminder_type_load_all');
  return $status;
}

/**
 * Deletes a reminder type.
 *
 * @param string $type
 *   The machine name of the reminder type.
 *
 * @throws Exception
 */
function reminder_type_delete($type) {
  entity_get_controller('reminder')->deleteBundle($type);
  drupal_static_reset('reminder_type_load_all');
}

/**
 * Returns an interval.
 *
 * @param int $name
 *   The unique identifier of the interval in the reminder type.
 * @param string $type
 *   The machine name of the reminder type.
 *
 * @return array|FALSE
 *   An associative array containing the interval, FALSE it it could not be
 *   loaded. See the return value of reminder_type_interval_extract() for the
 *   definition of what is returned by this function.
 *
 * @see reminder_type_interval_extract()
 */
function reminder_type_interval_load($name, $type) {
  if ($reminder_type = reminder_type_load($type)) {
    return reminder_type_interval_extract($name, $reminder_type);
  }
  return FALSE;
}

/**
 * Extracts an interval from a reminder type.
 *
 * @param $name
 *   The unique identifier of the interval in the reminder type.
 * @param stdClass $reminder_type
 *   The reminder type the interval is being extracted from.
 *
 * @return array|FALSE
 *   An associative array containing:
 */
function reminder_type_interval_extract($name, $reminder_type) {
  if (isset($reminder_type->intervals[$name])) {
    return $reminder_type->intervals[$name] + array('name' => $name);
  }
  return FALSE;
}
