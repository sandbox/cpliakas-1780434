<?php

/**
 * @file
 * Support module for reminder tests.
 */

/**
 * Stores data about implemented hooks.
 *
 * @param string $hook
 *   The name of the hook, i.e. "hook_foo".
 * @param array $args
 *   An array of arguments passed to the hook.
 * @param array $data
 *   Any additional data that should be stored.
 */
function reminder_test_set_implemented_hook($hook, array $args, array $data = array()) {
  $hooks = &drupal_static('reminder_tests_hooks', array());
  $hooks[$hook] = array(
    'implemented' => TRUE,
    'args' => $args,
    'data' => $data,
  );
}

/**
 * Returns information about the implemented hook.
 *
 * @param string $hook
 *   The name of the hook, i.e. "hook_foo".
 *
 * @return array
 *   An array of hook information.
 */
function reminder_test_get_implemented_hook($hook) {
  $hooks = &drupal_static('reminder_tests_hooks', array());
  if (isset($hooks[$hook])) {
    return $hooks[$hook];
  }
  else {
    return array(
      'implemented' => FALSE,
      'args' => array(),
      'data' => array(),
    );
  }
}

/**
 * Implements hook_reminder_type_delete().
 */
function reminder_test_reminder_type_delete($test) {
  reminder_test_set_implemented_hook('hook_reminder_type_delete', func_get_args());
}

/**
 * Implements hook_reminder_delivery_methods().
 */
function reminder_test_reminder_delivery_methods() {
  $delivery_methods = array();

  $delivery_methods['test'] = array(
    'label' => t('Test delivery method'),
    'delivery callback' => 'reminder_test_send_test',
  );

  return $delivery_methods;
}

/**
 * Delivery callback that logs context in a static variable.
 */
function reminder_test_send_test($message, array $recipients, $reminder, $entity) {
  $log = &drupal_static('reminder_test_send_log', array());
  $log[$reminder->rid] = array(
    'message' => $message,
    'recipients' => $recipients,
    'reminder' => $reminder,
    'entity' => $entity,
  );
}

/**
 * Returns the send log.
 */
function reminder_test_get_send_log() {
  return drupal_static('reminder_test_send_log', array());
}

/**
 * Implements hook_reminder_ENTITY_TYPE_date_fields() for node entities.
 */
function reminder_test_reminder_node_date_fields($bundle_name) {
  $fields = array();

  $fields['test_date'] = array(
    'label' => t('Test date field'),
    'value callback' => 'reminder_test_value_callback',
    'settings callback' => FALSE,
    'group' => t('Test property'),
  );

  return $fields;
}

/**
 * Implements hook_reminder_ENTITY_TYPE_recipient_fields() for node entities.
 */
function reminder_test_reminder_node_recipient_fields($bundle_name) {
  $fields = array();

  $fields['test_recipient'] = array(
    'label' => t('Test recipient field'),
    'value callback' => 'reminder_test_value_callback',
    'settings callback' => FALSE,
    'group' => t('Test property'),
  );

  return $fields;
}

/**
 * A dummy "value callback" that returns some text.
 */
function reminder_test_value_callback($entity, $field_name) {
  return t('Test value for @field', array('@field' => $field_name));
}
