<?php

/**
 * @file
 *
 */

/**
 *
 */
function reminder_type_list_page() {
  module_load_include('inc', 'node', 'content_types');
  $build = array();

  $header = array(
    array('data' => t('Name')),
    array('data' => t('Delivery method')),
    array('data' => t('Operations'), 'colspan' => 3)
  );
  $rows = array();

  $reminder_types = reminder_type_load_all();
  $delivery_methods = module_invoke_all('reminder_delivery_methods');
  foreach ($reminder_types as $type => $reminder_type) {
    $path = 'admin/config/workflow/reminder-types/manage/';
    $path .= reminder_encode_name($reminder_type->type);

    // Set the label and description.
    $variables = array('name' => $reminder_type->label, 'type' => $reminder_type);
    $row = array(theme('node_admin_overview', $variables));

    // Set the delivery type.
    if (isset($delivery_methods[$reminder_type->delivery_method])) {
      $delivery_method = $delivery_methods[$reminder_type->delivery_method]['label'];
    }
    else {
      $delivery_method = $reminder_type->delivery_method;
    }
    $row[] = array('data' => check_plain($delivery_method));

    // Set the edit column.
    $row[] = array('data' => l(t('edit'), $path));
    // Set the manage intervals column.
    $row[] = array('data' => l(t('manage intervals'), $path . '/intervals'));
    // Set the delete column.
    $row[] = array('data' => l(t('delete'), $path . '/delete'));

    $rows[] = $row;
  }

  $build['reminder_type_table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => reminder_type_empty_message(),
  );

  return $build;
}

/**
 *
 */
function reminder_type_form($form, &$form_state, $reminder_type = NULL) {
  if (NULL === $reminder_type) {
    $form['#is_new'] = TRUE;
    $reminder_type = reminder_type_new(TRUE);
  }
  else {
    $form['#is_new'] = FALSE;
  }

  $form['#reminder_type'] = $reminder_type;
  $form['#reminder_type_defaults'] = reminder_type_new();

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => $reminder_type->label,
    '#description' => t('The human-readable name of this reminder type.'),
    '#required' => TRUE,
    '#size' => 30,
  );

  $form['type'] = array(
    '#type' => 'machine_name',
    '#default_value' => $reminder_type->type,
    '#maxlength' => 32,
    '#disabled' => !$form['#is_new'],
    '#machine_name' => array(
      'exists' => 'reminder_type_load',
      'source' => array('label'),
    ),
  );

  $form['delivery_method'] = array(
    '#title' => t('Delivery method'),
    '#type' => 'select',
    '#options' => reminder_get_delivery_methods(),
    '#default_value' => $reminder_type->delivery_method,
    '#description' => t('Select the delivery method used to send the message.'),
  );

  $form['description'] = array(
    '#title' => t('Description'),
    '#type' => 'textarea',
    '#default_value' => $reminder_type->description,
    '#description' => t('A brief description of the reminder type used on administrative pages.'),
  );

  $form['intervals'] = array(
    '#type' => 'value',
    '#value' => $reminder_type->intervals,
  );

  $form['message'] = array(
    '#title' => t('Default message'),
    '#type' => 'textarea',
    '#required' => TRUE,
    '#default_value' => $reminder_type->message,
    '#description' => t('The reminder message used by all intervals that don\'t have an interval-specific message.'),
  );

  $form['token_tree'] = array(
    '#theme' => 'token_tree',
    '#token_types' => array(),
    '#show_restricted' => TRUE,
    '#access' => module_exists('token'),
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save and edit'),
    '#access' => !$form['#is_new'],
  );

  $form['actions']['submit_list'] = array(
    '#type' => 'submit',
    '#value' => $form['#is_new'] ? t('Save reminder type') : t('Save and return to list'),
  );

  $form['actions']['submit_interval'] = array(
    '#type' => 'submit',
    '#value' => t('Save and manage intervals'),
    '#access' => $form['#is_new'],
  );

  $form['actions']['cancel'] = array(
    '#type' => 'link',
    '#title' => t('Cancel'),
    '#href' => 'admin/config/workflow/reminder-types',
  );

  $form['#submit'][] = 'reminder_type_form_submit';
  return $form;
}

/**
 * Form submission handler for reminder_type_form().
 */
function reminder_type_form_submit($form, &$form_state) {
  $op = isset($form_state['values']['op']) ? $form_state['values']['op'] : '';

  // Build type, strip out all of the other stuff passed through the form.
  $reminder_type = array_intersect_key($form_state['values'], $form['#reminder_type_defaults']);

  $args = array('%label' => $form_state['values']['label']);
  try {
    reminder_type_save($reminder_type);
    if ($form['#is_new']) {
      drupal_set_message(t('The reminder type %label has been added.', $args));
      watchdog('reminder', 'Created reminder type %label.', $args, WATCHDOG_NOTICE);
    }
    else {
      drupal_set_message(t('The reminder type %label has been updated.', $args));
    }
  }
  catch (Exception $e) {
    if ($form['#is_new']) {
      drupal_set_message(t('Error adding reminder type %label.', $args), 'error');
    }
    else {
      drupal_set_message(t('Error updating reminder type %label.', $args), 'error');
    }
    return;
  }

  $type = reminder_encode_name($form_state['values']['type']);
  switch ($op) {
    case t('Save and edit'):
      $form_state['redirect'] = 'admin/config/workflow/reminder-types/manage/' . $type;
      break;

    case t('Save and manage intervals'):
      $form_state['redirect'] = 'admin/config/workflow/reminder-types/manage/' . $type . '/intervals';
      break;

    default:
      $form_state['redirect'] = 'admin/config/workflow/reminder-types';
      break;
  }
}

/**
 * Menu callback; delete a single content type.
 */
function reminder_type_delete_confirm($form, &$form_state, $reminder_type) {
  $form['type'] = array('#type' => 'value', '#value' => $reminder_type->type);
  $form['label'] = array('#type' => 'value', '#value' => $reminder_type->label);
  $message = t('Are you sure you want to delete the reminder type %type?', array('%type' => $reminder_type->type));
  $caption = '<p>' . t('This action cannot be undone.') . '</p>';
  return confirm_form($form, $message, 'admin/config/workflow/reminder-types', $caption, t('Delete'));
}

/**
 * Process content type delete confirm submissions.
 */
function reminder_type_delete_confirm_submit($form, &$form_state) {
  $args = array('%label' => $form_state['values']['label']);
  try {
    reminder_type_delete($form_state['values']['type']);
    drupal_set_message(t('The reminder type %label has been deleted.', $args));
    watchdog('reminder', 'Deleted reminder type %label.', $args, WATCHDOG_NOTICE);
  }
  catch (Exception $e) {
    drupal_set_message(t('Error deleting reminder type %label.', $args), 'error');
  }

  $form_state['redirect'] = 'admin/config/workflow/reminder-types';
}

/**
 *
 */
function reminder_type_interval_list_page($reminder_type) {
  $build = array();
  $type = reminder_encode_name($reminder_type->type);

  $header = array(
    'label' => array('data' => t('Label')),
    'interval' => array('data' => t('Interval')),
    'message' => array('data' => t('Has message')),
    'operations' => array('data' => t('Operations'), 'colspan' => 2),
  );

  $rows = array();
  foreach ($reminder_type->intervals as $key => $interval) {
    $path = 'admin/config/workflow/reminder-types/manage/' . $type . '/intervals/' . $key;
    $row = array();

    $row[] = array('data' => check_plain($interval['label']));
    $row[] = array('data' => check_plain($interval['interval']));
    $row[] = array('data' => $interval['has_message'] ? t('Yes') : t('Using default'));
    $row[] = array('data' => l(t('edit'), $path));
    $row[] = array('data' => l(t('delete'), $path . '/delete'));

    $rows[$key] = $row;
  }

  $add_link = url('admin/config/workflow/reminder-types/manage/' . $type . '/intervals/add');
  $build['reminder_type_table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('No intervals have been defined. <a href="@link">Add an interval</a>.', array('@link' => $add_link)),
  );

  return $build;
}

/**
 *
 *
 * @ingroup forms
 */
function reminder_type_interval_form($form, &$form_state, $reminder_type, $interval = NULL) {
  $form['#reminder_type'] = $reminder_type;
  $type = reminder_encode_name($reminder_type->type);

  if (NULL === $interval) {
    $form['#is_new'] = TRUE;
    $interval = array(
      'label' => '',
      'name' => '',
      'interval' => '',
      'has_message' => '',
      'message' => '',
      'key' => count($reminder_type->intervals),
    );
  }
  else {
    $form['#is_new'] = FALSE;
  }

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => $interval['label'],
    '#description' => t('The human-readable name of this interval.'),
    '#required' => TRUE,
    '#size' => 30,
  );

  $form['name'] = array(
    '#type' => 'machine_name',
    '#default_value' => $interval['name'],
    '#maxlength' => 32,
    '#disabled' => !$form['#is_new'],
    '#machine_name' => array(
      'exists' => 'reminder_type_interval_exists',
      'source' => array('label'),
    ),
  );

  $form['interval'] = array(
    '#type' => 'textfield',
    '#title' => t('Interval'),
    '#default_value' => $interval['interval'],
    '#description' => t('The interval in an English date format, i.e. +1 day, -30 days, etc.'),
    '#required' => TRUE,
  );

  $form['has_message'] = array(
    '#title' => t('Override the default message and add one specific to this interval.'),
    '#type' => 'checkbox',
    '#default_value' => $interval['has_message'],
  );

  $form['message'] = array(
    '#title' => t('Interval message'),
    '#type' => 'textarea',
    '#default_value' => $interval['message'],
    '#description' => t('The reminder message specific to this interval.'),
    '#states' => array(
      'visible' => array(
        ':input[name=has_message]' => array('checked' => TRUE),
      ),
    ),
  );

  $form['token_tree'] = array(
    '#theme' => 'token_tree',
    '#token_types' => array(),
    '#show_restricted' => TRUE,
    '#access' => module_exists('token'),
    '#states' => array(
      'visible' => array(
        ':input[name=has_message]' => array('checked' => TRUE),
      ),
    ),
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save interval'),
  );

  $form['actions']['cancel'] = array(
    '#type' => 'link',
    '#title' => t('Cancel'),
    '#href' => 'admin/config/workflow/reminder-types/manage/' . $type . '/intervals',
  );

  $form['#submit'][] = 'reminder_type_interval_form_validate';
  $form['#submit'][] = 'reminder_type_interval_form_submit';
  return $form;
}

/**
 *
 */
function reminder_type_interval_exists($name) {
  return reminder_type_interval_load($name, arg(5));
}

/**
 *
 */
function reminder_type_interval_form_validate($form, &$form_state) {
  if (FALSE === strtotime($form_state['values']['interval'])) {
    form_set_error('interval', t('Interval not valid'));
  }
}

/**
 *
 */
function reminder_type_interval_form_submit($form, &$form_state) {
  $reminder_type = $form['#reminder_type'];
  $type = reminder_encode_name($reminder_type->type);

  $name = $form_state['values']['name'];
  $reminder_type->intervals[$name] = array(
    'label' => $form_state['values']['label'],
    'interval' => $form_state['values']['interval'],
    'has_message' => $form_state['values']['has_message'],
    'message' => $form_state['values']['message'],
  );

  $args = array('%label' => $reminder_type->label);
  try {
    reminder_type_save($reminder_type);
    drupal_set_message(t('Interval added to reminder type %label.', $args));
  }
  catch (Exception $e) {
    drupal_set_message(t('Error adding interval to reminder type %label.', $args), 'error');
  }

  $form_state['redirect'] = 'admin/config/workflow/reminder-types/manage/' . $type . '/intervals';
}

/**
 *
 */
function reminder_type_interval_delete_confirm($form, &$form_state, $reminder_type, $interval) {
  $type = reminder_encode_name($reminder_type->type);
  $form['reminder_type'] = array('#type' => 'value', '#value' => $reminder_type);
  $form['interval'] = array('#type' => 'value', '#value' => $interval);
  $message = t('Are you sure you want to delete the interval %interval?', array('%interval' => $interval['interval']));
  $caption = '<p>' . t('This action cannot be undone.') . '</p>';
  $path = 'admin/config/workflow/reminder-types/manage/' . $type . '/intervals';
  return confirm_form($form, $message, $path, $caption, t('Delete'));
}

/**
 *
 */
function reminder_type_interval_delete_confirm_submit($form, &$form_state) {
  $reminder_type = $form_state['values']['reminder_type'];
  $interval = $form_state['values']['interval'];
  $type = reminder_encode_name($reminder_type->type);

  $args = array('%interval' => $interval['interval']);
  unset($reminder_type->intervals[$interval['key']]);
  try {
    reminder_type_save($reminder_type);
    drupal_set_message(t('The interval %interval has been deleted.', $args));
    watchdog('reminder', 'Deleted interval %interval.', $args, WATCHDOG_NOTICE);
  }
  catch (Exception $e) {
    drupal_set_message(t('Error deleting interval %interval.', $args), 'error');
  }

  $path = 'admin/config/workflow/reminder-types/manage/' . $type . '/intervals';
  $form_state['redirect'] = $path;
}

/**
 *
 */
function reminder_get_delivery_methods() {
  $options = array();
  $delivery_methods = module_invoke_all('reminder_delivery_methods');
  foreach ($delivery_methods as $name => $delivery_method) {
    $options[$name] = check_plain($delivery_method['label']);
  }
  return $options;
}
