<?php

/**
 * @file
 *
 */

/**
 *
 */
class ReminderController extends DrupalDefaultEntityController {

  /**
   *
   * @throws Exception
   */
  protected function getReminderIds(SelectQuery $query) {
    $rids = array();
    $result = $query->execute();
    foreach($result as $record) {
      $rids[$record->rid] = $record->rid;
    }
    return $rids;
  }

  /**
   *
   */
  public function loadByEntity($entity_type, $entity_id) {
    $reminders = array();
    try {

      $query = db_select('reminder', 'r')
        ->fields('r', array('rid'))
        ->condition('r.entity_type', $entity_type)
        ->condition('r.entity_id', $entity_id)
        ->orderBy('r.rid');

      if ($rids = $this->getReminderIds($query)) {
        $reminders = $this->load($rids);
      }

    }
    catch (Exception $e) {
      watchdog_exception('reminder', $e);
      return FALSE;
    }

    return $reminders;
  }

  /**
   *
   */
  public function loadPending($limit, $request_time) {
    $reminders = array();
    try {

      $query = db_select('reminder', 'r')
        ->fields('r', array('rid'))
        ->condition('r.status', REMINDER_STATUS_PENDING)
        ->condition('r.reminder_date', $request_time, '<=')
        ->orderBy('r.rid')
        ->range(0, $limit);

      // @todo add a hook to modify query?

      if ($rids = $this->getReminderIds($query)) {
        $reminders = $this->load($rids);
      }
    }
    catch (Exception $e) {
      watchdog_exception('reminder', $e);
      return FALSE;
    }

    return $reminders;
  }


  /**
   * Saves a reminder.
   *
   * @param stdClass $reminder
   *   The reminder object being saved.
   *
   * @throws Exception
   */
  public function save($reminder) {
    $transaction = db_transaction();
    try {

      // Load the stored entity, if any.
      if (!empty($reminder->rid) && !isset($reminder->original)) {
        $reminder->original = entity_load_unchanged('reminder', $reminder->rid);
      }

      // Determine if we will be inserting a new node.
      if (!isset($reminder->is_new)) {
        $reminder->is_new = empty($reminder->rid);
      }

      if (empty($reminder->log)) {
        $reminder->log = '';
      }

      $update_sid = TRUE;
      if ($reminder->is_new) {
        $op = 'insert';
        $reminder->created = REQUEST_TIME;
        $reminder->status = REMINDER_STATUS_PENDING;
        drupal_write_record('reminder', $reminder);
        drupal_write_record('reminder_status', $reminder);
      }
      else {
        $op = 'update';

        if (!empty($reminder->revision)) {
          $primary_keys = array();
          unset($reminder->sid);
          $reminder->created = REQUEST_TIME;
        }
        else {
          $primary_keys = 'sid';
          $update_sid = FALSE;
        }

        drupal_write_record('reminder', $reminder, 'rid');
        drupal_write_record('reminder_status', $reminder, $primary_keys);
      }

      if ($update_sid) {
        db_update('reminder')
          ->fields(array('sid' => $reminder->sid))
          ->condition('rid', $reminder->rid)
          ->execute();
      }

      module_invoke_all('entity_' . $op, $reminder, 'reminder');
      unset($reminder->is_new);
      unset($reminder->original);
      $this->resetCache(array($reminder->rid));

      // Ignore slave server temporarily to give time for the saved reminder to
      // be propagated to the slave.
      db_ignore_slave();
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog_exception('reminder', $e);
      throw $e;
    }
  }

  /**
   *
   * @param array $rids
   *   An array of reminder IDs to delete.
   *
   * @throws Exception
   */
  public function delete(array $rids) {
    $transaction = db_transaction();
    if (!empty($rids)) {
      try {
        db_delete('reminder')->condition('rid', $rids, 'IN')->execute();
        db_delete('reminder_status')->condition('rid', $rids, 'IN')->execute();
        // @todo delete queued items? I say no.
      }
      catch (Exception $e) {
        $transaction->rollback();
        watchdog_exception('reminder', $e);
        throw $e;
      }

      $this->resetCache();
    }
  }

  /**
   *
   */
  public function loadBundle($type) {
    $reminder_types = reminder_type_load_all();
    return isset($reminder_types[$type]) ? $reminder_types[$type] : FALSE;
  }

  /**
   * Saves a reminder type.
   *
   * @param array|stdClass $reminder_type
   *   The reminder type being saved.
   *
   * @return int
   *   The status flag indicating outcome of the operation. See the SAVED_*
   *   constants for possible values.
   */
  public function saveBundle($reminder_type) {
    $transaction = db_transaction();
    try {

      // Merge in defaults and strip extra stuff.
      $reminder_type = (array) $reminder_type;
      $defaults = reminder_type_new();
      $fields = $reminder_type + $defaults;
      $fields['intervals'] = serialize($fields['intervals']);

      // Update or insert depending on whether the reminder type exists.
      if (!$this->bundleExists($reminder_type['type'])) {
        db_insert('reminder_type')->fields($fields)->execute();
        field_attach_create_bundle('reminder', $fields['type']);
        $status = SAVED_NEW;
      }
      else {
        db_update('reminder_type')
          ->fields($fields)
          ->condition('type', $reminder_type['type'])
          ->execute();
        $status = SAVED_UPDATED;
      }

      return $status;
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog_exception('reminder', $e);
      throw $e;
    }
  }

  /**
   *
   * @param type $type
   */
  public function deleteBundle($type) {
    $transaction = db_transaction();
    try {
      db_delete('reminder_type')->condition('type', $type)->execute();
      module_invoke_all('reminder_type_delete', $type);
      field_attach_delete_bundle('reminder', $type);
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog_exception('reminder', $e);
      throw $e;
    }
  }

  /**
   * Checks whether the reminder type already exists.
   *
   * @param string $type
   *   The machine name of the reminder type.
   *
   * @return bool
   *   Whether or not the reminder type exists.
   *
   * @throws PDOException
   */
  public function bundleExists($type) {
    return (bool) db_query_range('SELECT 1 FROM {reminder_type} WHERE type = :type', 0, 1, array(':type' => $type))->fetchField();
  }
}