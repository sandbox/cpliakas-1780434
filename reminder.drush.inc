<?php

/**
 * @file
 * Drush commands provided by the Reminder module.
 */

/**
 * Implements hook_drush_command.
 */
function reminder_drush_command() {
  $items = array();

  $items['reminder-queue'] = array(
    'description' => 'Queues reminders for sending.',
    'options' => array(
      'limit' => 'The maximum number of items to queue.',
    ),
  );

  $items['reminder-send'] = array(
    'description' => 'Sends queued reminders.',
    'options' => array(
      'limit' => 'The maximum number seconds the queue process is allowed to run.',
    ),
  );

  $items['reminder-queue-count'] = array(
    'description' => 'Returns the number of reminders queued for sending.',
  );

  $items['reminder-pending-count'] = array(
    'description' => 'Returns the number of reminders waiting to be queued.',
  );

  return $items;
}

/**
 *
 */
function drush_reminder_queue() {
  $limit = drush_get_option('limit', 50);
  $num_queued = reminder_queue_reminders($limit);
  $message = format_plural($num_queued, '@count reminder was queued for sending.', '@count reminders were queued for sending.');
  drush_print($message);
}

/**
 *
 */
function drush_reminder_send() {
  $limit = drush_get_option('limit', 15);
  $num_sent = reminder_send_reminders($limit);
  $message = format_plural($num_sent, '@count reminder was sent.', '@count reminders were sent.');
  drush_print($message);
}

/**
 *
 */
function drush_reminder_queue_count() {
  try {
    drush_print(DrupalQueue::get('reminder')->numberOfItems());
  }
  catch (Exception $e) {
    drush_set_error('Error getting queue count.');
  }
}

/**
 *
 */
function drush_reminder_pending_count() {
  try {
    $args = array(':status' => REMINDER_STATUS_PENDING);
    $result = db_query('SELECT COUNT(rid) FROM {reminder} WHERE status = :status', $args);
    drush_print($result->fetchField());
  }
  catch (Exception $e) {
    drush_set_error('Error getting pending count.');
  }
}
