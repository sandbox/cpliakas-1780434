<?php

/**
 * @file
 *
 */

/**
 * Defines fields containing date values for an entity.
 *
 * @param string $bundle_name
 *   The machine name of the bundle date fields are attached to.
 *
 * @return array
 *   An associative array
 */
function hook_reminder_ENTITY_TYPE_date_fields($bundle_name) {

}

/**
 * Defines fields containing recipients the message will be sent to.
 *
 * @param string $bundle_name
 *   The machine name of the bundle date fields are attached to.
 *
 * @return array
 *   An associative array
 */
function hook_reminder_ENTITY_TYPE_recipient_fields($bundle_name) {

}

/**
 * Returns the recipient values for an entity.
 *
 * Wait a second... don't we have a function reminder_get_recipient_values()
 * that gets recipients from an entity? Yes, we do. However, each implementing
 * module, for example "Node Reminders", is responsible for storing the field
 * containing the recipients. Therefore this hook implementation usually just
 * gets the recipient field and calls reminder_get_recipient_values().
 *
 * It is important to note that only the hook implementation of the module that
 * created the reminder will be invoked when the reminder is being sent. The
 * module is stored in the reminder's "module" property.
 *
 * @param stdClass $entity
 *   The entity that the reminder references.
 *
 * @return array
 *   An array of recipients.
 *
 * @see reminder_get_recipient_values()
 */
function hook_reminder_recipient_values($entity) {

}

/**
 * Allows modules to react when a reminder type is deleted.
 *
 * @param string $type
 *   The machine name of reminder type being deleted.
 */
function hook_reminder_type_delete($type) {

}

/**
 *
 * @return array
 *   An array of deliver methods.
 *   - label:
 *   - delivery callback:
 */
function hook_reminder_delivery_methods() {

}

/**
 * Allows modules to alter the message sent to the recipient.
 *
 * @param string &$message
 *   The message that is being sent to the recipient.
 * @param string $recipient
 *   The recipient the message is being sent to.
 * @param stdClass $reminder
 *   The reminder entity.
 * @param stdClass $entity
 *   The entity that the reminder references.
 */
function hook_reminder_message_alter(&$message, $recipient, $reminder, $entity) {

}
